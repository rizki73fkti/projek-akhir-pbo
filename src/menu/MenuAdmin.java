package menu;

import data.DataPendaftar;
import static menu.Menu.clearScreen;
import static menu.Menu.tampilan;

import java.util.Scanner;

public class MenuAdmin {
    
    private int kesempatan_login = projekakhir.ProjekAkhir.getKesempatan_login();
    Menu menuawal;
    DataPendaftar dataAkun = new DataPendaftar();
    
    Scanner input = new Scanner(System.in);
    
    public MenuAdmin() {
        if ( kesempatan_login < 1 ) {
            System.out.println("Anda tidak dapat login sebagai admin.\n");
            menuawal = new Menu();
        } else {
            login();
        }
    }
    
    
    
    void login(){
        boolean loginLoop = true;
        while ( loginLoop ){	
            
            System.out.println("Kesempatan login: " + kesempatan_login );
            System.out.print("\nUsername: ");
            String id = input.nextLine();
            
            System.out.print("\nPassword: ");
            String pass = input.nextLine();

            if( id.equals("admin") && pass.equals("admin") ){
                    projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                    loginLoop = false;
                    tampilkanMenu();
                    
            }else{
                kesempatan_login -= 1;
                System.out.println(" Username / Password anda salah");
            }

            if( kesempatan_login == 0 ){
                loginLoop = false;
                projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                System.out.println("Anda tidak dapat login sebagai admin.\n");
                menuawal = new Menu();
            }
	}
    }
    
    void tampilkanMenu(){
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            System.out.println("| 1. Tambah Soal Tes                               |");
            System.out.println("| 3. Lihat Soal Tes                                |");
            System.out.println("| 3. Ubah Soal Tes                                 |");
            System.out.println("| 4. Lihat Data Akun                               |");
            System.out.println("| 5. Ubah Data Akun                                |");
            System.out.println("| 6. Hapus Akun                                    |");
            System.out.println("| 7. Kembali ke menu awal                          |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi = input.nextLine();
    
            switch (opsi) {
                case "1":
                    loop = false;
                    clearScreen();
                    System.out.println("***SOAL TES COMING SOON***");
                    tampilkanMenu();
                    break;
                    
                case "2":
                    loop = false;
                    clearScreen();
                    System.out.println("***LIHAT SOAL TES COMING SOON***");
                    tampilkanMenu();
                    break;
                    
                case "3":
                    loop = false;
                    clearScreen();
                    System.out.println("***UBAH SOAL TES COMING SOON***");
                    tampilkanMenu();
                    break;
                    
                case "4":
                    loop = false;
                    clearScreen();
                    dataAkun.lihatData();
                    tampilkanMenu();
                    break;
                    
                case "5":
                    loop = false;
                    clearScreen();
                     dataAkun.updateData();
                    tampilkanMenu();
                    break;
                    
                case "6":
                    loop = false;
                    clearScreen();
                     dataAkun.hapusData();
                    tampilkanMenu();
                    break;
                    
                case "7":
                    loop = false;
                    clearScreen();
                    menuawal = new Menu();
                    break;
                    
                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
            
        }
    }
    
}
